﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Atividade_Webforms
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void CheckBoxList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        /*aula 13 45:55*/
        protected void Button2_Click(object sender, EventArgs e)
        {
            lblClbResultadoNaTela.Text = "Os dias selecionados foram: <b />";
            for (int i = 0; i < chkDias.Items.Count; i++)
            {
                if (chkDias.Items[i].Selected)
                {
                    lblClbResultadoNaTela.Text += chkDias.Items[i].Text + "<br />";
                }
            }
        }

        protected void BtnCalcular_Click(object sender, EventArgs e)
        {
            // Obter os números dos TextBoxes
            double num1, num2;
            if (double.TryParse(txtNum1.Text, out num1) && double.TryParse(txtNum2.Text, out num2))
            {
                // Realizar o cálculo com base na operação selecionada
                string operacao = ddlOperacao.SelectedValue;
                double resultado = Calcular(num1, num2, operacao);

                // Exibir o resultado na Label
                lblResultado.Text = $"Resultado: {resultado}";
            }
            else
            {
                lblResultado.Text = "Por favor, insira números válidos.";
            }
        }

        private double Calcular(double num1, double num2, string operacao)
        {
            switch (operacao)
            {
                case "Soma":
                    return num1 + num2;
                case "Subtracao":
                    return num1 - num2;
                case "Multiplicacao":
                    return num1 * num2;
                case "Divisao":
                    if (num2 != 0)
                        return num1 / num2;
                    else
                        return double.NaN; // Divisão por zero
                case "Modulo":
                    if (num2 != 0)
                        return num1 % num2;
                    else
                        return double.NaN; // Divisão por zero
                default:
                    return double.NaN; // Operação inválida
            }
        }
        }
    }
