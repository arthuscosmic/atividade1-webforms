﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Atividade_Webforms.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Atividade 1</title>
    <style>
        body{
                 background-color: lightgray;
        }
        #calculadora{
            text-align: center;
        }

    </style>
    <script language="javascript">

    </script>
</head>
<body>
    <div class="caixa">
    <div id="calculadora">
    <form id="ChkCheckBox" runat="server">
        
            Calculadora<br />
            1º Numero <asp:TextBox ID="txtNum1" runat="server"></asp:TextBox>
            <br />
            2º Numero<asp:TextBox ID="txtNum2" runat="server"></asp:TextBox>
            <br />
            <asp:DropDownList ID="ddlOperacao" runat="server" Height="16px">
                <asp:ListItem>Soma</asp:ListItem>
                <asp:ListItem>Subtracao</asp:ListItem>
                <asp:ListItem>Multiplicacao</asp:ListItem>
                <asp:ListItem>Divisao</asp:ListItem>
                <asp:ListItem>Modulo</asp:ListItem>
            </asp:DropDownList>
            <br />
            <br />
            <asp:Button ID="BtnCalcular" runat="server" Text="Calcular" OnClick="BtnCalcular_Click" />
            <br />
            <asp:Label ID="lblResultado" runat="server"></asp:Label>

        </div>
        </div>
        <p>
            &nbsp;</p>
    <div class="caixa">    
    <div id="formulario">
        Formulário de trabalho<br />
        <asp:CheckBoxList ID="chkDias" runat="server" OnSelectedIndexChanged="CheckBoxList1_SelectedIndexChanged">
            <asp:ListItem>Segunda-Feira</asp:ListItem>
            <asp:ListItem>Terça-Feira</asp:ListItem>
            <asp:ListItem>Quarta-Feira</asp:ListItem>
            <asp:ListItem>Quinta-Feira</asp:ListItem>
            <asp:ListItem>Sexta-Feira</asp:ListItem>
            <asp:ListItem>Sábado</asp:ListItem>
            <asp:ListItem>Domingo</asp:ListItem>
        </asp:CheckBoxList>
        <asp:Button ID="btnVerificacao" runat="server" OnClick="Button2_Click" Text="Checar" />
       
    </form>
         <asp:Label ID="lblClbResultadoNaTela" runat="server"></asp:Label>
         </div>
         </div>
</body>
</html>
